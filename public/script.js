const postsContainer = document.getElementById('posts-container');
const loadingSpinner = document.getElementById('loading-spinner');
const loader = document.getElementById('loader');
let page = 1;
let isLoading = false;

async function fetchPosts() {
    if (isLoading) {
        return;
    }

    isLoading = true;
    loadingSpinner.style.display = 'block';

    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=10`);
        const posts = await response.json();

        isLoading = false;
        page++;

        if (posts.length === 0) {
            // No more posts to load
            loadingSpinner.style.display = 'none';
            return;
        }

        // Display the fetched posts
        posts.forEach(post => {
            const postElement = document.createElement('div');
            postElement.classList.add('post');
            postElement.innerHTML = `
                <h2>${post.title}</h2>
                <p>${post.body}</p>
            `;
            postsContainer.appendChild(postElement);
        });
    } catch (error) {
        console.error('Error fetching posts:', error);
    } finally {
        isLoading = false;
        loadingSpinner.style.display = 'none';
    }
}

function checkScroll() {
    const scrollY = window.scrollY;
    const windowHeight = window.innerHeight;
    const contentHeight = document.documentElement.scrollHeight;

    if (!isLoading && scrollY + windowHeight >= contentHeight - 100) {
        fetchPosts();
        loader.style.display = 'block'; // Show "Loading more..." text
    }
}

window.addEventListener('scroll', checkScroll);

// Initial load of posts
fetchPosts();
